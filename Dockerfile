# Usando uma imagem base do OpenJDK
FROM openjdk:11-jre-slim

# Criando o diretório de trabalho
WORKDIR /usr/src/app

# Copiando os arquivos do código-fonte para o diretório de trabalho no contêiner
COPY SistemaRota /usr/src/app/SistemaRota

# Compilando o código
RUN javac -d . SistemaRota/*.java

# Especificando o comando a ser executado quando o contêiner for iniciado
CMD ["java", "SistemaRota.TelaPrincipal"]